#! /usr/bin/env bash

rm -f signal.hepmc pileup.hepmc
mkfifo signal.hepmc pileup.hepmc

agile-runmc Pythia6:425 -n 10000 -o pileup.hepmc &
agile-runmc Pythia6:425 -n 10 -p MSEL=11 -o signal.hepmc &
pilemc -s signal.hepmc -p pileup.hepmc -o merged.hepmc -n 10 -v

