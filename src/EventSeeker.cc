#include "PileMC/EventSeeker.hh"
#include <string>

#include "boost/lexical_cast.hpp"

namespace PileMC{

  using std::string;
  using std::map;
  
  EventSeeker::EventSeeker(const string &inputFileName):
  _isGZip(HepMCIOHandler::isGZip(inputFileName)),
  _fileName(inputFileName), _hepmcFile((HepMC::IO_GenEvent*)0){
    _file = HepMCIOHandler::openInput(inputFileName);//(new ifstream(inputFileName, std::ios::in)),
    _hepmcFile = shared_ptr<HepMC::IO_GenEvent>(new HepMC::IO_GenEvent(*_file));
    
  }
  
  void EventSeeker::init(){
    
    if(_isGZip) return;
    
    if(!_file->is_open()) throw FileError(_fileName);
      
    std::streampos start = _file->tellg();
    
    _file->seekg(0);
    _firstEvent = findNext();
    _knownPositions[_firstEvent] = _file->tellg();
    
    _file->seekg(-1, _file->end);
    _lastEvent = findPrevious();
    _knownPositions[_lastEvent] = _file->tellg();
    
    _file->seekg(start);
    
    return;
  }
  
  int EventSeeker::minEvent()const{
    return _firstEvent;
  }
  
  int EventSeeker::maxEvent()const{
    return _lastEvent;
  }
  
  HepMC::IO_GenEvent &EventSeeker::hepmcFile(){
    return *_hepmcFile;
  }
  
  const string &EventSeeker::fileName()const{
    return _fileName;
  }
  
  bool EventSeeker::findEvent(int eventNumber){
    
    if(_isGZip){
      throw std::runtime_error("Cannot search gzipped input files!");
    }
    
    if(eventNumber < minEvent()){
      throw std::runtime_error("Requested event " + boost::lexical_cast<string>(eventNumber) + "is too small");
    }
    
    if(eventNumber > maxEvent()){
      throw std::runtime_error("Requested event " + boost::lexical_cast<string>(eventNumber) + "is too large");
    }
    
    map<int, std::streampos>::const_iterator lower = _knownPositions.upper_bound(eventNumber);
    map<int, std::streampos>::const_iterator upper = lower--;
    
    if(lower->first == eventNumber){
      _file->seekg(lower->second);
      return true;
    }
    
    bool adjacent = upper->first - lower->first == 2;
    
    std::streampos guess = (adjacent)? lower->second + std::streampos(3):
    lower->second +
    (upper->second - lower->second) * (eventNumber - lower->first) / (upper->first - lower->first);
    
    _file->seekg(guess);
    int event = findNext();
    
    if(event == upper->first){
      
      if(adjacent) return false;
      _file->seekg(guess);
      event = findPrevious();
      if(event == lower->first) return false;
    }
    
    _knownPositions[event] = _file->tellg();
    
    if(event == eventNumber) return true;
    
    findEvent(eventNumber);
    return true;
  }
  
  int EventSeeker::findNext(){

    string line;
    // read to the end of the line
    // unless already at the end of a line, in which case
    // don't read the next line
    if(_file->tellg() != _file->beg){
      _file->seekg(-1, _file->cur);
      if(_file->peek() != '\n') std::getline(*_file, line);
    }
    
    // continue reading lines until the first char is 'E'
    while(_file->peek() != 'E'){
      std::getline(*_file, line);
    }

    std::streampos pos = _file->tellg();
    _file->seekg(2, _file->cur);
    int event;
    *_file >> event;
    _file->seekg(pos);
    
    return event;
  }
  
  int EventSeeker::findPrevious(){
    
    bool found = false;
    char previous = _file->peek();
    
    while(!found){
      _file->seekg(-1, _file->cur);
      if(previous == 'E'){
        if(_file->peek() == '\n'){
          _file->seekg(1, _file->cur);
          found = true;
        }
      }
      
      previous = _file->peek();
    }
   
    std::streampos pos = _file->tellg();
    _file->seekg(2, _file->cur);
    int event;
    *_file >> event;
    _file->seekg(pos);
    
    return event;
  }
  
}










