#include "PileMC/HepMCIOHandler.hh"

namespace PileMC{
  
  IFilePtr HepMCIOHandler::openInput(const string &filename){
    
    if(isGZip(filename)) throw HepMCIOException(filename);
    
    return IFilePtr(new ifstream(filename, std::ios::in));
  }
  
  OFilePtr HepMCIOHandler::openOutput(const string &filename){
    return OFilePtr(new ofstream(filename.c_str(), std::ios::out));
  }
  
}
