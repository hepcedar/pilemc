/// This program equips HepMC events with pile-up. Both the "signal" events and
/// the pile-up events are read from HepMC streams. In turn, a HepMC out-stream
/// is generated.
///
/// @author Christian Roehr <roehr@particle.uni-karlsruhe.de>
/// @author Andy Buckley <andy.buckley@cern.ch>
/// @author James Monk <jmonk@cern.ch>

//#if HAVE_CONFIG_H
//#include <config.h>
//#endif

#include "ComposedEvent.h"
#include "PileMC/EventSeeker.hh"
#include "PileMC/HepMCIOHandler.hh"
#include "gsl/gsl_randist.h"
#include "tclap/CmdLine.h"

#include <boost/lexical_cast.hpp>
#include <boost/cast.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string.hpp>

#include <stdexcept>
#include <map>
#include <vector>
#include <set>

using namespace std;

typedef GenEvent::vertex_iterator vx_iterator;

int main( int argc, char** argv) {
  
  // Define the command line object
  TCLAP::CmdLine cmd("This program equips HepMC events with pile-up. Both "
                     "the 'signal' events and the 'pile-up' events are read from HepMC "
                     "streams. In turn, a HepMC output stream is generated.",
                     ' ', "0.1");
  
  // mean number of pile-up events to generate
  TCLAP::ValueArg<double> numberArg("n", "num-pileup",
                                    "Number of pile-up events to superimpose to one signal event. The meaning "
                                    "can change based on the sampling mode: the mean for Poisson sampling, the "
                                    "fixed value for const sampling, and the upper limit of a [0,n] range for "
                                    "uniform sampling. It should be an integer in the latter two cases, but may be a double"
                                    "for Poisson sampling. Default = 20.",
                                    false, 20., "double");
  cmd.add( numberArg );
  
  TCLAP::ValueArg<double> lowerArg("l", "lower-limit",
                                   "Lower limit to the number of pile up events.  This only applies to uniform sampling, so that the range is [l, n]",
                                   false, 0., "double");
  
  cmd.add(lowerArg);
  
  TCLAP::ValueArg<int> maxArg("m", "max-events",
                              "Maximum number of events to generate.  Default to all events in signal file",
                              false, -1, "int");
  cmd.add(maxArg);
  
  TCLAP::ValueArg<int> seedArg("r", "random-seed",
                               "Random seed",
                               false, 140280, "int");
  
  cmd.add(seedArg);
  
  // sample mode for number of pile-up events: you can use fixed N, flat-sampled N, or a Poisson distribution
  TCLAP::ValueArg<string> sampletypeArg("", "sample-type",
                                        "Type of sampling used to determine the number of pile-up vertices: [poisson, const, uniform]. "
                                        "Default = poisson.",
                                        false, "poisson", "typename");
  cmd.add( sampletypeArg );
  
  // abuse the GenEvent::mpi() member to record the number of pile-up events
  TCLAP::SwitchArg abusempiSwitch("", "abuse-mpi", "Abuse the mpi() member of the output "
                                  "event to record the number of pile-up interactions",
                                  cmd, false);
  
  TCLAP::SwitchArg flitArg("f", "flit-about", "move randomly around the input pile up files in order to select events at random.  This allows large <mu> overlays to be generated from relavtively small input samples, since the same input event can be overlaid on many output events", cmd, false);
  
  // Gaussian beam spot z width in mm to sample from when translating events
  TCLAP::ValueArg<double> zwidthArg("z", "zwidth",
                                    "Gaussian beam spot z width in mm, from which to sample collision offsets",
                                    false, 0.0, "width in mm");
  cmd.add( zwidthArg );
  
  // the output HepMC file
  TCLAP::ValueArg<string> outputArg("o", "output",
                                    "Path of the output HepMC file containing signal + pile-up",
                                    true, "defaultvalue", "filename");
  cmd.add( outputArg );
  
  // the pile-up HepMC file
  TCLAP::MultiArg<string> pileupArg("p", "pileup",
                                    "Path to HepMC file(s) containing the pile-up events",
                                    true, "filename");
  cmd.add( pileupArg );
  
  // the signal HepMC file
  TCLAP::ValueArg<string> signalArg("s", "signal",
                                    "Path to HepMC file containing the signal events",
                                    false, "", "filename");
  cmd.add( signalArg );
  
  // be verbose or not
  TCLAP::SwitchArg verboseSwitch("v", "verbose",
                                 "Switch to verbose mode", cmd,
                                 false);
  
  // Parse the argv array.
  cmd.parse( argc, argv );
  
  
  // Specify the input files and the ascii output file
  

  string outfile    = outputArg.getValue();
  
  std::vector<std::vector<std::string> > fractions;
  
  double xsSum = 0.;
  size_t checkLength = 0;
  for(auto &filename: pileupArg.getValue()){
    fractions.push_back(std::vector<std::string>());
    boost::split(fractions.back(), filename, boost::is_any_of(":"));
    if(fractions.back().size() > 2){
      throw std::runtime_error("pile up file specified with multiple ':'");
    }
    if(!checkLength)checkLength = fractions.back().size();
    if(fractions.back().size() != checkLength){
      throw std::runtime_error("All pile up files must be specified with either just a name, or a name and cross section separated by :.  You cannot mix these modes");
    }
    
    if(checkLength == 2){
      xsSum += boost::lexical_cast<double>(fractions.back()[1]);
    }else{
      fractions.back().push_back("1.");
      xsSum += 1.;
    }
  }
  
  double fraction = 0.;
  std::map<double, PileMC::EventSeeker> pileupInputs;
  std::map<double, std::set<int> > pileupEventNumbers;
  for(auto &filename: fractions){
    fraction += boost::lexical_cast<double>(filename[1]) / xsSum;
    pileupInputs.emplace(fraction, filename[0]);
    pileupInputs.rbegin()->second.init();
    pileupEventNumbers[fraction] = std::set<int>();
  }
  
  // Get remaining arguments
  bool verbose = verboseSwitch.getValue();
  
  // check the number of pile up events, and convert to integer if necessary
  
  double d_pileUp = numberArg.getValue();
  
  if(d_pileUp < 0) throw std::runtime_error("Negative number of pile-up events not allowed!!");
  
  int i_pileUp = 0;
  
  //Only convert to integer if sampling is not poisson
  if(sampletypeArg.getValue() != "poisson"){
    i_pileUp = boost::numeric_cast<int>(d_pileUp);
    
    double test = fmod(d_pileUp, (double)i_pileUp);
    if(test > 1e-06)throw std::runtime_error("You have chosen a non-integer mu."
                                             "  Floating point mu is only available with Poisson sampling."
                                             "Your chosen value was " + boost::lexical_cast<std::string>(d_pileUp));
  }
  
  HepMC::IO_GenEvent *ascii_in_signal = 0;
  
  PileMC::IFilePtr signalFile(0);
  
  if(signalArg.getValue() !=""){
    signalFile = PileMC::HepMCIOHandler::openInput(signalArg.getValue());// new std::ifstream(signalArg.getValue(), std::ios::in);
    /*
    if ( ! signalFile->is_open() ){
      std::cerr << "Cannot find " << signalArg.getValue() << std::endl << "Aborting!" << std::endl;
      return 2;
    }
    */
    ascii_in_signal = new HepMC::IO_GenEvent(*signalFile);
    ascii_in_signal->read_next_event();
  }else if(maxArg.getValue() < 0){
    std::cerr << "Must set max events with -m or --max-events when there is no signal file"<<std::endl;
    return 2;
  }
  
  HepMC::IO_GenEvent ascii_out( *(PileMC::HepMCIOHandler::openOutput(outfile)));//outfile,    ios::out);
  
  int eventCounter = 0;
  
  // Create a random number generator
  const gsl_rng * rng = gsl_rng_alloc (gsl_rng_taus2);
  gsl_rng_set(rng, seedArg.getValue());
  
  // Loop until we run out of signal events
  while (eventCounter != maxArg.getValue()) {
    
    int n_pileup = 0;
    // This is really the sum of n pile up if the input events already have pile up
    int mpiSum = 0;
    
    if (sampletypeArg.getValue() == "poisson") {
      n_pileup = gsl_ran_poisson(rng, d_pileUp);
    }else if(sampletypeArg.getValue() == "uniform"){
      double tmp = gsl_ran_flat(rng, lowerArg.getValue(), d_pileUp);
      n_pileup = boost::numeric_cast<int>(tmp);
    }else if(sampletypeArg.getValue() == "const"){
      n_pileup = i_pileUp;
    }
    
    if(n_pileup == 0) continue;
    
    for(std::map<double, std::set<int> >::iterator r = pileupEventNumbers.begin();
        r != pileupEventNumbers.end(); ++r){
      r->second.clear();
    }
    
    HepMC::GenEvent *evt = 0;
    
    if(ascii_in_signal != 0){
      evt = ascii_in_signal->read_next_event();
    }else{
      
      std::map<double, PileMC::EventSeeker>::iterator pupSeeker;
      std::map<double, std::set<int> >::iterator numRecord;
      if(pileupInputs.size() > 1){
        double r = gsl_ran_flat(rng, 0., 1.);
        pupSeeker = pileupInputs.upper_bound(r);
        numRecord = pileupEventNumbers.upper_bound(r);
      }else{
        pupSeeker = pileupInputs.begin();
        numRecord = pileupEventNumbers.begin();
      }
      
      if(pupSeeker == pileupInputs.end()){
        throw std::runtime_error("Could not find pile up file");
      }
      
      bool findEvent = flitArg.getValue();
      
      while(findEvent){
        int eventNumber = int(gsl_ran_flat(rng, pupSeeker->second.minEvent(), pupSeeker->second.maxEvent() + 1.));
        findEvent = !pupSeeker->second.findEvent(eventNumber);
      }
      pupSeeker->second.hepmcFile() >> evt;
      --n_pileup;
    }
    
    if(!evt) {
      std::cout<<"no signal events"<<std::endl;
      break;
    }
    
    // Create a composite event and delete the original to free up memory
    ComposedEvent compEvt(evt);
    delete evt;
    
    int counter = 0;
    while(n_pileup != counter){
      
      std::map<double, PileMC::EventSeeker>::iterator pupSeeker;
      std::map<double, std::set<int> >::iterator numRecord;
      if(pileupInputs.size() > 1){
        double r = gsl_ran_flat(rng, 0., 1.);
        pupSeeker = pileupInputs.upper_bound(r);
        numRecord = pileupEventNumbers.upper_bound(r);
      }else{
        pupSeeker = pileupInputs.begin();
        numRecord = pileupEventNumbers.begin();
      }
      
      if(pupSeeker == pileupInputs.end()){
        throw std::runtime_error("Could not find pile up file");
      }
      
      bool findEvent = flitArg.getValue();
      
      while(findEvent){
        int eventNumber = int(gsl_ran_flat(rng, pupSeeker->second.minEvent(), pupSeeker->second.maxEvent() + 1.));
        if(numRecord->second.count(eventNumber) != 0)continue;
        findEvent = !pupSeeker->second.findEvent(eventNumber);
        if(!findEvent) numRecord->second.insert(eventNumber);
      }
      
      HepMC::GenEvent* pileupevt = 0;
      pupSeeker->second.hepmcFile() >> pileupevt;
      
      if(!pileupevt){// we have run out of pile up inputs
        break;
      }
      
      if(pileupevt->mpi() > 0){
        mpiSum += pileupevt->mpi();
      }else{
        mpiSum += 1;
      }
      
      ++counter;
      if ( verbose )
        cerr << "--> processing pileup event #" << counter << " from " << pupSeeker->second.fileName() << " ...\n";
      
      // Do beamspot vertex smearing in z around 0.0 for pile-up events
      if ( zwidthArg.getValue() > 0.0 ) {
        double zoffset = gsl_ran_gaussian( rng, zwidthArg.getValue() );
        // Handle units -- width is sampled in mm
        if (pileupevt->length_unit() == HepMC::Units::CM) zoffset /= 10.0;
        if ( verbose )
          cerr << "z-offset: " << zoffset << " mm" << endl;
        for (HepMC::GenEvent::vertex_iterator v = pileupevt->vertices_begin(); v != pileupevt->vertices_end(); ++v) {
          const HepMC::FourVector& v4 = (*v)->position();
          (*v)->set_position(HepMC::FourVector(v4.x(), v4.y(), v4.z() + zoffset, v4.t()));
        }
      }
      
      // Add this pile-up event and delete the original to free up memory
      compEvt.addEvent( pileupevt );
      delete pileupevt;
    }
    
    if(n_pileup != counter) break;
    
    // Get the superimposed uber-event, and abuse the n_MPI data member
    // to represent pile-up, if that was requested by the user
    GenEvent* sImpos = compEvt.superimposition();
    if ( abusempiSwitch.getValue() ) {
      if(ascii_in_signal == 0) ++n_pileup;
      sImpos->set_mpi(mpiSum);
    }
    
    // Dump out the superimposed event
    if ( verbose )
      cerr << "--> writing pile-up-enriched event to " << outfile << "\n";
    ascii_out << sImpos;
    
    ++eventCounter;

  }
  
  return 0;
}

