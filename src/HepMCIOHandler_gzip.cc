#include "PileMC/HepMCIOHandler.hh"

#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string.hpp>

#include <vector>

namespace PileMC{
  
  using std::vector;
  
  IFilePtr HepMCIOHandler::openInput(const string &filename){
    
    
    if(isGZip(filename)){
      boost::iostreams::filtering_istream *filter_istr =
      new boost::iostreams::filtering_istream();
      
      filter_istr->push(boost::iostreams::gzip_decompressor());
      filter_istr->push(*(new ifstream(filename, std::ios::in)));
      
      return IFilePtr((ifstream*)filter_istr);
      
    }
    
    return IFilePtr(new ifstream(filename, std::ios::in));
  }
  
  OFilePtr HepMCIOHandler::openOutput(const string &filename){
    
    vector<string> substrs;
    boost::split(substrs, filename, boost::is_any_of("."));
    
    if(substrs.back() != string("gz")){
      return OFilePtr(new ofstream(filename.c_str(), std::ios::out));
    }
    
    boost::iostreams::filtering_ostream *filter_ostr =
    new boost::iostreams::filtering_ostream();
    filter_ostr->push(boost::iostreams::gzip_compressor());
    filter_ostr->push(*(new ofstream(filename.c_str(), std::ios::out)));
    
    return OFilePtr((ofstream*)filter_ostr);
  }
  
}

