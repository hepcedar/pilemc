#ifndef COMPOSEDEVENT_H
#define COMPOSEDEVENT_H

#include "HepMC/GenEvent.h"
#include "HepMC/IO_GenEvent.h"
using namespace HepMC;

/// @brief A container for a signal event + pile-up events
///
/// This class is designed for superimposing multiple HepMC events resulting in a
/// single HepMC event. This can be used to simulate additional pile-up, i.e.
/// beam-beam collisions that occur during the same bunch crossing.
///
/// So far, there is no possibility to alter the spacetime information of pile-up
/// events. For more sophisticated studies a proper modelling of the beam shape
/// should probably be implemented.
///
/// @author Christian Roehr <roehr@particle.uni-karlsruhe.de>
class ComposedEvent {
public:

  /// Constructor
  ComposedEvent(const GenEvent* signalEvent);

  /// Destructor
  ~ComposedEvent() {}

  /// Get the superimposed GenEvent
  GenEvent* superimposition() {
    return &m_superimposition;
  }

  /// Add a (pile-up) event to the superimposition
  void addEvent(const GenEvent* inevent, bool issignal=false);

private:

  /// Event containing vertices and particles from both signal and pile-up
  GenEvent m_superimposition;

};

#endif

