#include "ComposedEvent.h"


ComposedEvent::ComposedEvent(const GenEvent* signalEvent) {
  addEvent(signalEvent, true);

  m_superimposition.set_signal_process_id( signalEvent->signal_process_id() );
  m_superimposition.set_event_number( signalEvent->event_number() );
  m_superimposition.set_event_scale( signalEvent->event_scale() );
  m_superimposition.set_alphaQCD( signalEvent->alphaQCD() );
  m_superimposition.set_alphaQED( signalEvent->alphaQED() );
  m_superimposition.set_mpi( signalEvent->mpi() );

  GenCrossSection* cs(0);
  if ( signalEvent->cross_section() ) {
    cs = new GenCrossSection( *signalEvent->cross_section() );
    m_superimposition.set_cross_section( *cs );
  }

  PdfInfo* pdfinfo(0);
  if ( signalEvent->pdf_info() ) {
    pdfinfo = new PdfInfo( *signalEvent->pdf_info() );
    m_superimposition.set_pdf_info( *pdfinfo );
  }
}


void ComposedEvent::addEvent(const GenEvent* inevent, bool issignal ) {

  std::map<const GenVertex*,GenVertex*> map_in_to_new;

  const int barcode_offset = issignal ? 0 : 1000000;

  // add vertices
  for ( GenEvent::vertex_const_iterator v = inevent->vertices_begin();
      v != inevent->vertices_end(); ++v ) {
    GenVertex* newvertex = new GenVertex(
        (*v)->position(), (*v)->id(), (*v)->weights() );
    newvertex->suggest_barcode( -barcode_offset + (*v)->barcode() );
    map_in_to_new[*v] = newvertex;
    m_superimposition.add_vertex( newvertex );
  }

  // if inevent is a signal event copy the signal process vertex info
  if ( issignal ) {
    if (!inevent->signal_process_vertex()) {
      m_superimposition.set_signal_process_vertex(
          map_in_to_new[inevent->signal_process_vertex()] );
    } else m_superimposition.set_signal_process_vertex( 0 );
  }

  /// @todo If pile-up, only copy the (beams and) final-state particles

  // copy the particles from inevent and attach them to the correct vertex
  GenParticle* beam1(0);
  GenParticle* beam2(0);
  for ( GenEvent::particle_const_iterator p = inevent->particles_begin();
      p != inevent->particles_end(); ++p )
  {
    GenParticle* oldparticle = *p;
    GenParticle* newparticle = new GenParticle(*oldparticle);
    newparticle->suggest_barcode(oldparticle->barcode() + barcode_offset);
    if ( oldparticle->end_vertex() ) {
      map_in_to_new[ oldparticle->end_vertex() ]->
        add_particle_in(newparticle);
    }
    if ( oldparticle->production_vertex() ) {
      map_in_to_new[ oldparticle->production_vertex() ]->
        add_particle_out(newparticle);
    }
    if ( oldparticle == inevent->beam_particles().first ) beam1 = newparticle;
    if ( oldparticle == inevent->beam_particles().second ) beam2 = newparticle;
  }
  if ( issignal ) {
    m_superimposition.set_beam_particles( beam1, beam2 );
  }

  // now that vtx/particles are copied, copy weights and random states
  m_superimposition.set_random_states( inevent->random_states() );
  m_superimposition.weights() = inevent->weights();
}
