#ifndef PILEMC_HEPMCIOHANDLER_HH
#define PILEMC_HEPMCIOHANDLER_HH

#include <fstream>
#include <iostream>
#include <memory>
#include <string>
#include <vector>
#include <stdexcept>

namespace PileMC{
  
  using std::shared_ptr;
  using std::ifstream;
  using std::ofstream;
  using std::string;
  using std::vector;
  
//  typedef shared_ptr<ifstream> IFilePtr;
  typedef ifstream* IFilePtr;
  typedef ofstream * OFilePtr;
  
  class HepMCIOException : public std::runtime_error{
  public:
    HepMCIOException(const string &filename): std::runtime_error("File " + filename + "cannot be opened"){
      
    }
    
  };
  
  class HepMCIOHandler{
    
  public:
    
    static IFilePtr openInput(const string &filename);
    
    static OFilePtr openOutput(const string &filename);
    
    static bool isGZip(const string &filename){
      std::ifstream infile (filename, std::ios::binary | std::ios::in);
      
      if(!infile.is_open())throw HepMCIOException(filename);
      
      unsigned char buffer[2];
      infile.read((char*)buffer, 2);
      bool gzip =   ((int)buffer[0]) == 31 && ((int)buffer[1]) == 139;
      
      infile.close();
      return gzip;
    }
    
  };
}

#endif
