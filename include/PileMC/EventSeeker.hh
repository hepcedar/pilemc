#ifndef PILEMC_EVENT_SEEKER_HH
#define PILEMC_EVENT_SEEKER_HH

#include "PileMC/HepMCIOHandler.hh"

#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <stdexcept>
#include <memory>

#include "HepMC/IO_GenEvent.h"

namespace PileMC{
  
  using std::string;
  using std::ifstream;
  using std::map;
  using std::shared_ptr;
  
  class FileError : public std::runtime_error{
  public:
    FileError(const string &fileName):std::runtime_error("Input file " + fileName + "is not open!"){}
  };
  
  /// Seeks a specified event number from a HepMC file
  /**
   *  This class holds a HepMC ifstream and records the locations of 
   *  numbered events within that file.  It builds a map of the line
   *  numbers of requested events.  For events that are not in the map,
   *  the location is first guessed based on the length of the file
   *  and the number of the event
   */
  class EventSeeker{
    
  public:
    
    EventSeeker(const string &inputFileName);
    
    void init();
    
    bool findEvent(int eventNumber);
    
    int minEvent()const;
    int maxEvent()const;
    
    HepMC::IO_GenEvent &hepmcFile();
    
    const string &fileName()const;
    
  private:
    
    int findNext();
    int findPrevious();
    
    bool _isGZip;
    string _fileName;
    // need pointers for files so that EventSeeker may be copyable
    IFilePtr _file;
    shared_ptr<HepMC::IO_GenEvent> _hepmcFile;
    
    map<int, std::streampos> _knownPositions;
    
    int _firstEvent;
    int _lastEvent;
  };
  
}

#endif
